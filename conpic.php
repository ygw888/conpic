<?php
// +----------------------------------------------------------------------
// | YGWMVC [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2019-2023 https://www.ysdns.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

class SimpleImage
{

	/**
	 * 图片资源
	 * @var
	 */
	protected $image;
	
	/**
	 * 海报背景高度
	 * @var
	 */
	protected $backgroundHeight;
	
	/**
	 * 海报背景高度
	 * @var
	 */
	protected $backgroundWidth;
	
	/**
	 * 字体颜色
	 * @var string
	 */
	protected $fontColor = '#ffffff';
	
	/**
	 * 描边颜色
	 * @var string
	 */
	protected $strokeColor = '#000000';
	
	/**
	 * 描边粗细
	 * @var string
	 */
	protected $strokePx = 3;
	
	/**
	 * 字体大小
	 * @var string
	 */
	protected $fontSize = "46";
	
	/**
	 * 图片路径
	 * @var
	 */
	protected $imgPath;

	/**
	 * 字体路径
	 */
	protected $fontPath;
	
	/**
	 * 行距
	 */
	protected $hangSize = 66;
	
	/**
	 * 顶部距离
	 */
	protected $top = 0;

	/**
	 * 全局图库
	 */
	protected $position = 0;
	
	/**
	 * 图库
	 */
	protected $picDir;
	
	/**
	 * 图片资源是否可以保存
	 * @var bool
	 */
	protected $saveStatus = true;
	
	/**
	 * 图片类型
	 * @var
	 */
	protected $mime;
	
	/**
	 * 构造方法
	 * @var bool
	 */
	function __construct() {
	   $this->fontColor = $this->hex2rgb($this->fontColor);
	   $this->strokeColor = $this->hex2rgb($this->strokeColor);
	   $this->imgPath = $this->downloadImage();
	   $this->fontPath = $this->downloadFont();
	}

	/**
	 * 获取图片资源
	 * @param string $url
	 * @return array
	 */
	public function downloadImage()
	{
	    $imgcont = $this->curl('https://www.ysdns.com/index.php?s=httpapi&id=3&appid=1&appsecret=PHPCMF8AE1ACDFE9580');
	    $pic_path = json_decode($imgcont,true);
	    $pic_path = $pic_path['data'];
	    $pic_key = array_rand($pic_path,1);
	    $pic_path = $pic_path[$pic_key]['thumb'];
// 		if(is_dir($picDir)){
// 			$datadir = scandir($picDir);
// 		}
// 		$datadir = array_slice($datadir,2);
// 		$imgcont = rand(0,count($datadir));
// 		$pic_path =$this->picDir.$datadir[$imgcont];
		return $pic_path;
	}

	/**
	 * 获取字体资源
	 * @param string $url
	 * @return array
	 */
	public function downloadFont()
	{
		$fontdir = './font/';
		if(is_dir($fontdir)){
			$fontdir = scandir($fontdir);
		}
		$fontdir = array_slice($fontdir,2);
		$font_key = array_rand($fontdir,1);
		$font_path ='./font/'.$fontdir[$font_key];
		return $font_path;
	}
	
	/**
	 * 文字自动换行算法
	 * @param $card 画板
	 * @param $pos 数组，top距离画板顶端的距离，fontsize文字的大小，width宽度，left距离左边的距离，hang_size行高
	 * @param $str 要写的字符串
	 * @param $iswrite  是否输出，ture，  花出文字，false只计算占用的高度
	 * @return int 返回整个字符所占用的高度
	 */
	
	public function setText($str, $iswrite, $uploadsfile = '', $base64 = 1){
	    $_str_h = $this->top;
	    $fontsize = $this->fontSize;
	    $temp_string = "";
	    $font_file = $this->fontPath;
	    $tp = 0;
	    $px = $this->strokePx;
	    $pic_path = $this->imgPath;
	    $font_color = imagecolorallocate($this->image, $this->fontColor["r"], $this->fontColor["g"], $this->fontColor["b"]);
	    $stroke_color = imagecolorallocate($this->image, $this->strokeColor["r"], $this->strokeColor["g"], $this->strokeColor["b"]);
		$ext = pathinfo($this->imgPath, PATHINFO_EXTENSION);
	    //图片信息
	    list($pic_w, $pic_h, $pic_type) = getimagesize($pic_path);
	    imagesavealpha($this->image,true);//这里很重要 意思是不要丢了图像的透明色;
	    $width = $pic_w * 0.7;
	    $margin_lift = $x = $pic_w * 0.15;
	    $hang_size = $this->hangSize;
	    $myx = $this->position;
	    for ($i = 0; $i < mb_strlen($str); $i++) {
	        $box = imagettfbbox($fontsize, 0, $font_file, $temp_string);
	        $_string_length = $box[2] - $box[0];
	        $temptext = mb_substr($str, $i, 1);
	        $temp = imagettfbbox($fontsize, 0, $font_file, $temptext);
	        if ($_string_length + $temp[2] - $temp[0] < $width) {//长度不够，字数不够，需要继续拼接字符串。
	            $temp_string .= mb_substr($str, $i, 1);
	            if ($i == mb_strlen($str) - 1) {//是不是最后半行。不满一行的情况
	                $_str_h += $hang_size;//计算整个文字换行后的高度。
	                $tp++;//行数
	                //是否需要写入，核心绘制函数
	                if ($iswrite) {
						$ttfdata = $this->calculateTextBox($temp_string,$font_file,$fontsize,0);
						if($ttfdata && !$myx){
							$x = ceil(($pic_w - $ttfdata['width'])/2);
						}
						if($px){
							for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++){
								for($c2 = ($_str_h-abs($px)); $c2 <= ($_str_h+abs($px)); $c2++){

									$bg = imagettftext($this->image, $fontsize, 0, $c1, $c2, $stroke_color, $font_file, $temp_string);

								}
							}
							imagettftext($this->image, $fontsize, 0, $x, $_str_h, $font_color, $font_file, $temp_string);

						}else{
							imagettftext($this->image, $fontsize, 0, $x, $_str_h, $font_color, $font_file, $temp_string);
						}
	                }
	                //是否需要写入，核心绘制函数
	            }
	        } else {//一行的字数够了，长度够了。

	            $ttfdata = $this->calculateTextBox($temp_string,$font_file,$fontsize,0);
	            if($ttfdata && !$myx){
	                $x = ceil(($pic_w - $ttfdata['width'])/2);
	            }
	
				//           打印输出，对字符串零时字符串置null
	            $texts = mb_substr($str, $i, 1);//零时行的开头第一个字。
				//            判断默认第一个字符是不是符号；
	            $isfuhao = preg_match("/[\\\\pP]/u", $texts) ? true : false;//一行的开头这个字符，是不是标点符号
	            if ($isfuhao) {//如果是标点符号，则添加在第一行的结尾
	                $temp_string .= $texts;
					//                判断如果是连续两个字符出现，并且两个丢失必须放在句末尾的，单独处理
	                $f = mb_substr($str, $i + 1, 1);
	                $fh = preg_match("/[\\\\pP]/u", $f) ? true : false;
	                if ($fh) {
	                    $temp_string .= $f;
	                    $i++;
	                }
	            } else {
	                $i--;
	            }
	            $tmp_str_len = mb_strlen($temp_string);
	            $s = mb_substr($temp_string, $tmp_str_len-1, 1);//取零时字符串最后一位字符
	                if ($this->is_firstfuhao($s)) {//判断零时字符串的最后一个字符是不是可以放在见面
	                    //讲最后一个字符用“_”代替。指针前移动一位。重新取被替换的字符。
	                    $temp_string=rtrim($temp_string,$s);
	                    $i--;
	                }
						//            }
					//            计算行高，和行数。
	            $_str_h += $hang_size;
	            $tp++;
	            if ($iswrite) {
	                    if($px){
	                        for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++){
	                            for($c2 = ($_str_h-abs($px)); $c2 <= ($_str_h+abs($px)); $c2++){
	                                $bg = imagettftext($this->image, $fontsize, 0, $c1, $c2, $stroke_color, $font_file, $temp_string);
	                            }
	                        }
	                        imagettftext($this->image, $fontsize, 0, $x, $_str_h, $font_color, $font_file, $temp_string);
	
	                    }else{
	                        imagettftext($this->image, $fontsize, 0, $x, $_str_h, $font_color, $font_file, $temp_string);
	                    }
	            }
				//           写完了改行，置null该行的临时字符串。
	            $temp_string = "";
	        }
	    }
	    if ($iswrite) {
			//按照画布类型输出图片
			$pngName = '/uploadfile/thumb/'.time().rand(0,9).".".$ext;//生成图片名称
			if($uploadsfile){
				switch ($this->mime) {
					case 1://GIF
						if(!$base64){
							imagegif($this->image);
						}else{
							imagegif($this->image,WEBPATH.$pngName);
							return dr_url_prefix($pngName);
						}
						break;
					case 2://JPG
						if(!$base64){
							imagejpeg($this->image,$uploadsfile);
						}else{
							imagejpeg($this->image,$uploadsfile);
						}
						break;
					case 3://PNG
						if(!$base64){
							imagepng($this->image);
						}else{
				
							imagepng($this->image,WEBPATH.$pngName);
							return dr_url_prefix($pngName);
						}
						break;
					default:
						break;
				}
				if (!file_exists($uploadsfile)) {
					throw new Exception("图片生成失败");
				}
				//  释放掉图片资源
				imagedestroy($this->image);
				return $this;
			}else{
				switch ($this->mime) {
					case 1://GIF
						if(!$base64){
							header('Content-Type: image/gif;charset=utf-8');
							imagegif($this->image);
						}else{
							imagegif($this->image,WEBPATH.$pngName);
							return dr_url_prefix($pngName);
						}
						break;
					case 2://JPG
						if(!$base64){
							header('Content-Type: image/jpeg;charset=utf-8');
							imagejpeg($this->image);
						}else{
							header('Content-Type: image/jpeg;charset=utf-8');
							imagejpeg($this->image);
						}
						break;
					case 3://PNG
						if(!$base64){
							header('Content-Type: image/png;charset=utf-8');
							imagepng($this->image);
						}else{
				
							imagepng($this->image,WEBPATH.$pngName);
							return dr_url_prefix($pngName);
						}
						break;
					default:
						break;
				}
				imagedestroy($this->image);
				return $this;
			}
			/*
	        if(!$base64){
	            //按照画布类型输出图片
	            switch ($pic_type) {
	                case 1://GIF
	                    header('Content-Type: image/gif;charset=utf-8');
	                    imagegif($card);
	                    break;
	                case 2://JPG
	                    header('Content-Type: image/jpeg;charset=utf-8');
	                    imagejpeg($card);
	                    break;
	                case 3://PNG
	                    header('Content-Type: image/png;charset=utf-8');
	                    // 保存图片路径
	                    imagepng($card);
	                    break;
	                default:
	                    break;
	            }
	            imagedestroy($card);
	            exit;
	
	        }else{
	
	            switch ($pic_type) {
	                case 1://GIF
	                    //header('Content-Type: image/gif');
	                    imagegif($card,WEBPATH.$pngName);
	                    return SITE_URL.$pngName;
	                    break;
	                case 2://JPG
	                    //header('Content-Type: image/jpeg');
	                    imagejpeg($card,WEBPATH.$pngName);
	                    return SITE_URL.$pngName;
	                    break;
	                case 3://PNG
	                    //header('Content-Type: image/png');
	                    
	                    // 保存图片路径
	                    imagepng($card,WEBPATH.$pngName);
	                    return SITE_URL.$pngName;
	                    break;
	                default:
	                    break;
	            }
	            imagedestroy($card);
	
	        }
		*/
	    }else{
	        return $tp * $hang_size;
	    }

	}
	
	public function curl($url,$params=false,$ispost=false,$https=true,$cookies=0){
    	$UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; .NET CLR 3.5.21022; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';  
        $ch = curl_init();//初始化一个cURL会话。
        curl_setopt($ch, CURLOPT_URL, $url);//设置抓取的url
        curl_setopt($ch, CURLOPT_HEADER, 0);//启用时会将头文件的信息作为数据流输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//TRUE 将curl_exec()获取的信息以字符串返回，而不是直接输出。
        if($https === true){
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 跳过https请求 不验证证书和hosts
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        if($ispost){
        	curl_setopt($ch,CURLOPT_POST,true);//开启发送post请求选项
    		curl_setopt($ch,CURLOPT_POSTFIELDS,$params);//发送post的数据
        }
    	if($cookies){
    		curl_setopt($ch, CURLOPT_COOKIE, $cookies);
    	}
        curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
      	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);// 设置超时限制防止死循环
        $data = curl_exec($ch);   
        curl_close($ch); 
        //$data = json_decode($data,true);
        return $data;
    }
	
	/**
	* 十六进制 转 RGB
	*/
	public function hex2rgb($hexColor) {
		$color = str_replace('#', '', $hexColor);
		if (strlen($color) > 3) {
		  $rgb = array(
			'r' => hexdec(substr($color, 0, 2)),
			'g' => hexdec(substr($color, 2, 2)),
			'b' => hexdec(substr($color, 4, 2))
		  );
		} else {
		  $color = $hexColor;
		  $r = substr($color, 0, 1) . substr($color, 0, 1);
		  $g = substr($color, 1, 1) . substr($color, 1, 1);
		  $b = substr($color, 2, 1) . substr($color, 2, 1);
		  $rgb = array(
			'r' => hexdec($r),
			'g' => hexdec($g),
			'b' => hexdec($b)
		  );
		}
		return $rgb;
	}
	
	public function calculateTextBox($text,$fontFile,$fontSize,$fontAngle) {   
		$rect = imagettfbbox($fontSize,$fontAngle,$fontFile,$text);
		$minX = intval(min(array($rect[0],$rect[2],$rect[4],$rect[6])));
		$maxX = intval(max(array($rect[0],$rect[2],$rect[4],$rect[6])));
		$minY = intval(min(array($rect[1],$rect[3],$rect[5],$rect[7])));
		$maxY = intval(max(array($rect[1],$rect[3],$rect[5],$rect[7])));

		return array(
		 "left"   => abs($minX) - 1,
		 "top"    => abs($minY) - 1,
		 "width"  => $maxX - $minX,
		 "height" => $maxY - $minY,
		 "box"    => $rect
		);
	}
	    
	    
	public function is_firstfuhao($str) {
		$fuhaos = array("\"", "“", "'", "<", "《",);
		return in_array($str, $fuhaos);
	}
	
	/**
	 * 输出图片到网页
	 * @date (2022/4/11 17:18:22)
	 * @author (WangDaBao)
	 */
	public function show($title,$uploadsfile = '')
	{
		list($pic_w, $pic_h, $pic_type) = getimagesize($this->imgPath);
		$this->mime = $pic_type;
		$this->backgroundHeight = $pic_h;
		$this->backgroundWidth = $pic_w;
		//创建图片的实例
		switch ($this->mime) {
			case 1://GIF
				$this->image = imagecreatefromgif($this->imgPath);
				break;
			case 2://JPG
				$this->image = imagecreatefromjpeg($this->imgPath);
				break;
			case 3://PNG
				$this->image = imagecreatefrompng($this->imgPath);
				break;
			default:
				break;
		}
		header("content-type: " . $this->mime);
		if (!$this->image) {
			throw new Exception("图片资源为空");
		}
		$str_h = $this->setText($title, false, 0);
		$this->top = ($this->backgroundHeight - $str_h) / 2;
		return $this->setText($title, true, $uploadsfile, 0);
	}

	/**
	 * 保存图片
	 * @date (2022/4/11 17:32:49)
	 * @param      $filename (图片存放路径)
	 * @param int  $compression (图片质量)
	 * @param null $permissions
	 * @author (WangDaBao)
	 */
	public function save($compression = 75)
	{
		$filename = './uploadfile';
		
	}

}
